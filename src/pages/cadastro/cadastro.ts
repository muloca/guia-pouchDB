import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { TabsPage } from '../tabs/tabs';


@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  estados:any;

  cadastro: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public database: DatabaseProvider,
    private alertCtrl: AlertController) {

      database.dbLocal.changes({
        since: 'now',
        live: true,
        include_docs: false
      }).on('change', (change)=>{
        this.ionViewDidLoad();
      });

      this.cadastro = this.formBuilder.group({
        nome: ['', [Validators.required, Validators.minLength(3)]],
        cidade: ['', [Validators.required, Validators.minLength(3)]],
        telefone: ['', [Validators.minLength(10)]]
      })
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Obrigado!',
      subTitle: 'Navegue a vontade.',
      buttons: ['Começar']
    });
    alert.present();
  }


  ionViewDidLoad() {
    // let loading = this.showLoading()
    this.database.getEstados().then((data) =>{
      this.estados = data['docs'];
      //loading.dismiss();
    }).catch((err)=> {
      //loading.dismiss();
      alert('erro')
      console.log(err)
    })
  }

  paginaInicial() {
    this.navCtrl.setRoot(HomePage);
  }

  onSubmit() {
    this.navCtrl.setRoot(TabsPage)
  }

}
