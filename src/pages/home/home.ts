import { Component } from '@angular/core';
import { NavController, ModalController, Loading, LoadingController } from 'ionic-angular';
import { HomeCidadesPage } from './homeCidades';
import { DatabaseProvider } from '../../providers/database/database';

import { HomeSearchPage } from './homeSearch';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  estados:any;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public database: DatabaseProvider,
    public loadingCtrl: LoadingController
    
  ) {

    let loading = this.showLoading()


      database.dbLocal.changes({
        since: 'now',
        live: true,
        include_docs: false
      }).on('change', (change)=>{
        this.ionViewDidEnter();
      });
      
  }

  
  public searchClientes() {
    let modal = this.modalCtrl.create(HomeSearchPage);
    modal.present();
  }

  ionViewDidEnter(){
    this.database.getEstados().then((data) =>{
      this.estados = data['docs'];
    }).catch((err)=> {
      alert('erro')
      console.log(err)
    })
  }


  openHomeCidades(estado){
    this.navCtrl.push(HomeCidadesPage,{estado:estado});
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde os estados serem carregados...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 6000);
    return loading;
  }

}
