import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

import { HomeSearchPage } from './homeSearch';


@Component({
  selector: 'page-home-search-result',
  templateUrl: 'homeSearchResult.html'
})
export class HomeSearchResultPage {
  clientes:any;
  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public modalCtrl: ModalController,
    public database: DatabaseProvider) {

      database.dbLocal.changes({
        since: 'now',
        live: true,
        include_docs: false
      }).on('change', (change)=>{
        this.ionViewDidEnter();
      });
  }

  ionViewDidEnter(){
      var cliente = this.params.get('cliente');
      var reg = cliente.nomeCliente;
      this.database.getClientes({
        nome:{'$regex':new RegExp(reg,'g')},
      }).then((dataCliente) =>{
        console.log(dataCliente);
        this.clientes = dataCliente['docs'];
      });
  }

  goBack(){
    this.navCtrl.pop();
  }

  public searchClientes() {
    let modal = this.modalCtrl.create(HomeSearchPage);
    modal.present();
  }
}
