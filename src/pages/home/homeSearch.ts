import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';

import { HomeSearchResultPage } from './homeSearchResult';

@IonicPage()
@Component({
  selector: 'page-home-search',
  templateUrl: 'homeSearch.html',
})
export class HomeSearchPage {
  cliente= {
    nomeCliente:""
  };

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public databaseService: DatabaseProvider,) {
    
  }

  ionViewDidEnter(){
  }

  public dismiss(){
    this.navCtrl.pop();
  }

  public searchClientes() {
    this.navCtrl.pop();
    this.navCtrl.push(HomeSearchResultPage,{cliente:this.cliente});
  };

}
