import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomeRamosPage } from './homeRamos';

import { DatabaseProvider } from '../../providers/database/database';


@Component({
  selector: 'page-home-cidade',
  templateUrl: 'homeCidades.html'
})
export class HomeCidadesPage {
  cidades:any;

  constructor(
    public navCtrl: NavController,
    public params: NavParams,
    public database: DatabaseProvider
  ) {

    database.dbLocal.changes({
      since: 'now',
      live: true,
      include_docs: false
    }).on('change', (change)=>{
      this.ionViewDidEnter();
    });
  }

  ionViewDidEnter(){
      var estado = this.params.get('estado');
      this.database.getCidades({estado:estado._id}).then((dataCidade) =>{
        this.cidades = dataCidade['docs'];
      });
  }
  openHomeRamos(cidade){
    this.navCtrl.push(HomeRamosPage,{cidade:cidade});
  }
}
