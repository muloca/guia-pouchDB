import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DatabaseProvider } from '../../providers/database/database';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  estados:any;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public database: DatabaseProvider) {

    database.dbLocal.changes({
      since: 'now',
      live: true,
      include_docs: false
    }).on('change', (change)=>{
      this.ionViewDidEnter();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
  }

  ionViewDidEnter(){
    // let loading = this.showLoading()
     this.database.getEstados().then((data) =>{
       this.estados = data['docs'];
       //loading.dismiss();
     }).catch((err)=> {
       //loading.dismiss();
       alert('erro')
       console.log(err)
     })
   }

  paginaInicial() {
    this.navCtrl.setRoot(HomePage);
  }

}
