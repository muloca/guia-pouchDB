import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { FbProvider } from './../../providers/firebase/fb';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-promocoes',
  templateUrl: 'promocoes.html',
})
export class PromocoesPage {

  contacts: Observable<any>;
  contact: any;
  promocoes: Observable<any>;
  promo: any;
  pet;

  constructor(
    public navCtrl: NavController,
    private provider: FbProvider
  ) {

      this.pet = "kittens";

      this.contacts = this.provider.getCupons();
      this.promocoes = this.provider.getPromocoes();
  }
  
}
