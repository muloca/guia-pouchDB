import { Component } from '@angular/core';

import { InformacoesPage } from '../informacoes/informacoes';
import { PromocoesPage } from '../promocoes/promocoes';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = PromocoesPage;
  tab3Root = InformacoesPage;

  constructor() {

  }
}