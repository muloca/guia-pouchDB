import { Injectable } from '@angular/core';


@Injectable()
export class ConfigProvider {

  private config = {
    showCadastro: false,
    nome: "",
    cidade: "",
    telefone: ""
  }

  constructor() {

  }

  getConfigData() {
    return localStorage.getItem("config");
  }

  setConfigData(showCadastro?: boolean, nome?: string, cidade?: string, telefone?: string) {

    let config = {
      showCadastro: false,
      nome: "",
      cidade: "",
      telefone: ""
    };

    if(showCadastro){
      config.showCadastro = showCadastro
    }

    if(nome){
      config.nome = nome
    }

    if(cidade){
      config.cidade = cidade
    }

    if(telefone){
      config.telefone = telefone
    }

    localStorage.setItem("config", JSON.stringify(config));
  }

}
