import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class FbProvider {
  private PATH = 'cidades/';
  private PATH2 = 'cupons/';
  private PATH3 = 'ramos/';
  private PATH4 = 'clientes/';
  private estadoPATH = 'estados/';
  private promoPATH = 'promocoes/';

  constructor(private db: AngularFireDatabase) {
  }

  getAll() {
    return this.db.list(this.PATH, ref => ref.orderByChild('cidade'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

  getEstado() {
    return this.db.list(this.estadoPATH, ref => ref.orderByChild('estado'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }
  

  getRamos(cidade) {

    return this.db.list(this.PATH3, ref => 
      ref.orderByChild('ramo'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }
  getClientes(ramo) {

    return this.db.list(this.PATH4, ref => 
      ref.orderByChild('nome'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

  getCupons() {
    return this.db.list(this.PATH2, ref => 
      ref.orderByChild('cupom'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

  getPromocoes() {
    return this.db.list(this.promoPATH, ref => 
      ref.orderByChild('promocao'))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

  get(key: string) {
    return this.db.object(this.PATH + key).snapshotChanges()
      .map(c => {
        return { key: c.key, ...c.payload.val() };
      });
  }

}
