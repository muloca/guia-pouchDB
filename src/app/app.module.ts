import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HomeCidadesPage } from '../pages/home/homeCidades';
import { HomeRamosPage } from '../pages/home/homeRamos';
import { HomeClientesPage } from '../pages/home/homeClientes';

import { DatabaseProvider } from '../providers/database/database';






import { TabsPage } from '../pages/tabs/tabs';
import { PromocoesPage } from '../pages/promocoes/promocoes';
import { InformacoesPage } from '../pages/informacoes/informacoes';


import { HomeSearchPage } from '../pages/home/homeSearch';
import { HomeSearchResultPage } from '../pages/home/homeSearchResult';
import { CadastroPageModule } from '../pages/cadastro/cadastro.module';

import { FbProvider } from '../providers/firebase/fb';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

export const firebaseConfig = {
  apiKey: "AIzaSyCER9fVeR_sJfFl6hYugs6hmHrmT6qu8ro",
  authDomain: "ecolistadb.firebaseapp.com",
  databaseURL: "https://ecolistadb.firebaseio.com",
  projectId: "ecolistadb",
  storageBucket: "",
  messagingSenderId: "157553156027"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomeCidadesPage,
    HomeRamosPage,
    HomeClientesPage,
    HomeSearchPage,
    HomeSearchResultPage,
    PromocoesPage,
    InformacoesPage,
    TabsPage
  ],
  imports: [
    CadastroPageModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      platforms: {
        ios: {
      backButtonText: 'Voltar',
      iconMode: 'ios',
      pageTransition: 'ios-transition'}
      }
    },
  ),
  AngularFireModule.initializeApp(firebaseConfig),
  AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomeCidadesPage,
    HomeRamosPage,
    HomeClientesPage,
    HomeSearchPage,
    HomeSearchResultPage,
    PromocoesPage,
    InformacoesPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    FbProvider
  ]
})
export class AppModule {}
